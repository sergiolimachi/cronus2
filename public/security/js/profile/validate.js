$(document).ready(function () {
    $('#profile_form')
        // IMPORTANT: You must declare .on('init.field.bv')
        // before calling .bootstrapValidator(options)
        .on('init.field.bv', function (e, data) {
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group'),
                $icon = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]');

            // From v0.5.3, you can retrieve the icon element by
            // $icon = data.element.data('bv.icon');

            $icon.on('click.clearing', function () {
                // Check if the field is valid or not via the icon class
                if ($icon.hasClass('glyphicon-remove')) {
                    // Clear the field
                    data.bv.resetField(data.element);
                }
            });
        })

        .bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'greicoex_grxseucirtybundle_user_update[firstname]': {
                    validators: {
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        }
                    }
                },
                'greicoex_grxseucirtybundle_user_update[lastname]': {
                    validators: {
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        }
                    }
                },
                'greicoex_grxseucirtybundle_user_update[phoneNumber]': {
                    validators: {
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        },
                        numeric: {
                            message: Translator.trans('Only numbers')
                        }
                    }
                },
                'greicoex_grxseucirtybundle_user_update[email]': {
                    validators: {
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        },
                        emailAddress: {
                            message: Translator.trans('The value is not a valid email address')
                        }
                    }
                }
            }
        });


    $('#form-change-password')
        // IMPORTANT: You must declare .on('init.field.bv')
        // before calling .bootstrapValidator(options)
        .on('init.field.bv', function (e, data) {
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group'),
                $icon = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]');

            // From v0.5.3, you can retrieve the icon element by
            // $icon = data.element.data('bv.icon');

            $icon.on('click.clearing', function () {
                // Check if the field is valid or not via the icon class
                if ($icon.hasClass('glyphicon-remove')) {
                    // Clear the field
                    data.bv.resetField(data.element);
                }
            });
        })

        .bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'fos_user_change_password_form[current_password]': {
                    validators: {
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        }
                    }
                },
                'fos_user_change_password_form[plainPassword][first]': {
                    validators: {
                        stringLength: {
                            min: 8,
                            message: Translator.trans('The password must be more than 8 characters')
                        },
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        }
                    }
                },
                'fos_user_change_password_form[plainPassword][second]': {
                    validators: {
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        },
                        identical: {
                            field: 'fos_user_change_password_form[plainPassword][first]',
                            message: Translator.trans('The password and its confirm are not the same')
                        }
                    }
                }
            }
        });

});
