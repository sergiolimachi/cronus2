function sendNotify(title,text,type)
{
    new PNotify({
        title: title,
        text: text,
        type: type
    });
}