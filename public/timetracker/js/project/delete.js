function deleteProject(url)
{
    $.get( url, function( data ) {
        $('#confirm-delete').modal('hide');
        loadProjectList();
        var msg = Translator.trans('The project has been deleted successfully');
        sendNotify('Info',msg, 'success');
    });
}