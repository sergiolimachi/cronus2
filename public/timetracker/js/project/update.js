function editProject(id, name,customerId){
    var projectModal = $('#projectModal');
    projectModal.find('#id').val(id);
    projectModal.find('#name').val(name);
    projectModal.find('#customers').prop('checked', false);
    projectModal.find('input,select').removeProp('disabled');
    loadCustomers(customerId);
    $("#customer_content").show();
    projectModal.find('#saveProject').show();
    projectModal.modal('show');
}