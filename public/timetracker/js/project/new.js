function loadCustomers(customerId)
{
    $("#customers_div").html('');
    $.get('/customer/list/available/', function( response ) {
        var msgASelect = Translator.trans('Select a customer');
        var html ="<select id='customers' name='customer' class='form-control'><option value='0'>"+msgASelect+"</option> </select>";
        $("#customers_div").append(html);
        if(typeof response.empty === 'undefined')
        {
            $.each(response.data, function(key, value) {
                if(customerId == value.id)
                {
                    $('#customers')
                        .show()
                        .append($("<option></option>")
                            .attr("value",value.id)
                            .attr("selected","selected")
                            .text(value.name));
                }else{
                    $('#customers')
                        .show()
                        .append($("<option></option>")
                            .attr("value",value.id)
                            .text(value.name));
                }
            });
            $("#customers_div").show();
        }else{
           // $('#customers').hide();
            $("#customers_div").show();
        }
        var msgAdd = Translator.trans('Add New Customer');
        html = "<span id='helpBlock' class='help-block'><a data-toggle='modal' href='#newCustomerModal' >"+msgAdd+"</a></span>";
        $("#customers_div").append(html);
    });

}



function newProject(){
    loadCustomers(0);//llamamos al metodo de arriba
    var projectModal = $('#projectModal');
    projectModal.find('#id').val('');
    projectModal.find('#name').val('');
    projectModal.find('#email').val('');
    $("#customer_content").show();//id Contenedor
    projectModal.find('#customers').prop('checked', false);
    projectModal.find('input,select').removeProp('disabled');
    projectModal.find('#saveProject').show();
    projectModal.modal('show');
}

function saveProject(){
    var data = $('#projectForm').serialize();
    var selected = [];
    $("#projects  option:selected").each(function () {
        selected.push($(this).val());
    });

    if(selected.length > 0)
    {
        var dataProjects = {
            'projects' : selected
        };
        data += '&' + $.param(dataProjects);
    }

    $.ajax({
        type: 'POST',
        url: $('#projectForm').attr('action'),
        data: data,
        beforeSend: function(jqXHR, settings){
            //loadStart();
        },
        success: function(data, textStatus, jqXHR){
            $('#projectModal').modal('hide');
            loadProjectList();
            $('#projectForm').bootstrapValidator('resetForm', true);

            var msg = Translator.trans('The project has been added successfully');
            if($('#id').val() > 0 )
            {
                msg = Translator.trans('The project has been updated successfully');
            }

            sendNotify('Info',msg, 'success');
        },
        error: function(jqXHR, textStatus, errorThrown){
            var msg = Translator.trans('Could not complete the action');
            sendNotify('Info',msg, 'error');
        },
        complete: function(jqXHR, textStatus){
            //loadStop();
        }
    });
}


$(document).ready(function() {

    $('#projectForm')
        // IMPORTANT: You must declare .on('init.field.bv')
        // before calling .bootstrapValidator(options)
        .on('init.field.bv', function(e, data) {
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group'),
                $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]');

            // From v0.5.3, you can retrieve the icon element by
            // $icon = data.element.data('bv.icon');

            $icon.on('click.clearing', function() {
                // Check if the field is valid or not via the icon class
                if ($icon.hasClass('glyphicon-remove')) {
                    // Clear the field
                    data.bv.resetField(data.element);
                }
            });
        })
        .bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        }
                    }
                }
            }
        })
        .on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            saveProject();
        });

});


function saveCustomer(){
    var data = $('#newCustomerForm').serialize();
    var selected = [];
    $("#projects  option:selected").each(function () {
        selected.push($(this).val());
    });

    if(selected.length > 0)
    {
        var dataProjects = {
            'projects' : selected
        };
        data += '&' + $.param(dataProjects);
    }

    $.ajax({
        type: 'POST',
        url: $('#newCustomerForm').attr('action'),
        data: data,
        beforeSend: function(jqXHR, settings){
            //loadStart();
        },
        success: function(data, textStatus, jqXHR){
            $('#customers')
                .show()
                .append($("<option selected='selected'></option>")
                    .attr("value",data.id)
                    .text(data.name));
            $('#newCustomerModal').modal('hide');
            $('#newCustomerForm').bootstrapValidator('resetForm', true);
            var msg = Translator.trans('The customer has been added successfully');
            sendNotify('Info',msg, 'success');
        },
        error: function(jqXHR, textStatus, errorThrown){
            var msg = Translator.trans('Could not complete the action');
            sendNotify('Info',msg, 'error');
        },
        complete: function(jqXHR, textStatus){
            //loadStop();
        }
    });
}


$(document).ready(function() {

    $('#newCustomerForm')
        // IMPORTANT: You must declare .on('init.field.bv')
        // before calling .bootstrapValidator(options)
        .on('init.field.bv', function(e, data) {
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group'),
                $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]');

            // From v0.5.3, you can retrieve the icon element by
            // $icon = data.element.data('bv.icon');

            $icon.on('click.clearing', function() {
                // Check if the field is valid or not via the icon class
                if ($icon.hasClass('glyphicon-remove')) {
                    // Clear the field
                    data.bv.resetField(data.element);
                }
            });
        })
        .bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        }
                    }
                }
            }
        })
        .on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            saveCustomer();
        });

});