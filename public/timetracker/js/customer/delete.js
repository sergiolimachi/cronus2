function deleteCustomer(url)
{
    $.get( url, function( data ) {
        $('#confirm-delete').modal('hide');
        loadCustomerList();
        var msg = Translator.trans('The customer has been deleted successfully');
        sendNotify('Info',msg, 'success');
    });
}