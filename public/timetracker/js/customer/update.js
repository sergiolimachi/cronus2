function editCustomer(id, name, email){
    loadProjects(id);
    var customerModal = $('#customerModal');
    customerModal.find('#id').val(id);
    customerModal.find('#name').val(name);
    customerModal.find('#email').val(email);
    customerModal.find('#projects').prop('checked', false);
    customerModal.find('input,select').removeProp('disabled');
    $("#project_content").show();
    customerModal.find('#saveCustomer').show();
    customerModal.modal('show');
}