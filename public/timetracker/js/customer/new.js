function loadProjects(cId)
{
    $("#projects_div").html('');
    var url = Routing.generate('project_without_customer_list',{customerId:cId});
    $.get( url, function( response ) {
        var html ="<select id='projects' multiple='multiple'> </select>";
        $("#projects_div").append(html);
        if(typeof response.empty === 'undefined')
        {
            $.each(response.data, function(key, value) {

                if(typeof value.selected === 'undefined')
                {
                    $('#projects')
                        .show()
                        .append($("<option></option>")
                            .attr("value",value.id)
                            .text(value.name));
                }else{
                    $('#projects')
                        .show()
                        .append($("<option></option>")
                            .attr("value",value.id)
                            .attr("selected","selected")
                            .text(value.name));
                }


            });
            $('#projects').multiselect({
                selectAllValue: 'multiselect-all',
                enableCaseInsensitiveFiltering: true,
                enableFiltering: true,
                maxHeight: '300',
                buttonWidth: '235'
            });
            $("#projects_div").show();
       }else{
          // $('#projects').hide();

            $('#projects').multiselect({
                selectAllValue: 'multiselect-all',
                enableCaseInsensitiveFiltering: true,
                enableFiltering: true,
                maxHeight: '300',
                buttonWidth: '235'
            });
           $("#projects_div").show();
        }
        //html = "<button type='button' class='btn btn-primary'>Add Project</button> ";
        var msgAdd = Translator.trans('Add New Project');
        html = "<span id='helpBlock' class='help-block'><a data-toggle='modal' href='#newProjectModal' >"+msgAdd+"</a></span>";
        $("#projects_div").append(html);
    });

}

function newCustomer(){
    loadProjects(0);
    var customerModal = $('#customerModal');
    customerModal.find('#id').val('');
    customerModal.find('#name').val('');
    customerModal.find('#email').val('');
    $("#project_content").show();
    customerModal.find('#projects').prop('checked', false);
    customerModal.find('input,select').removeProp('disabled');
    customerModal.find('#saveCustomer').show();
    customerModal.modal('show');
}

function saveCustomer(){
    var data = $('#customerForm').serialize();
    var selected = [];
    $("#projects  option:selected").each(function () {
        selected.push($(this).val());
    });

    if(selected.length > 0)
    {
        var dataProjects = {
            'projects' : selected
        };
        data += '&' + $.param(dataProjects);
    }
    $.ajax({
        type: 'POST',
        url: $('#customerForm').attr('action'),
        data: data,
        beforeSend: function(jqXHR, settings){
            //loadStart();
        },
        success: function(data, textStatus, jqXHR){
            $('#customerModal').modal('hide');
            loadCustomerList();
            $('#customerForm').bootstrapValidator('resetForm', true);

            var msg = Translator.trans('The customer has been added successfully');
            if($('#id').val() > 0 )
            {
                msg = Translator.trans('The customer has been updated successfully');
            }

            sendNotify('Info',msg, 'success');
        },
        error: function(jqXHR, textStatus, errorThrown){
            var msg = Translator.trans('Could not complete the action');
            sendNotify('Info',msg, 'error');
        },
        complete: function(jqXHR, textStatus){
            //loadStop();
        }
    });
}

$(document).ready(function() {

    $('#customerForm')
        // IMPORTANT: You must declare .on('init.field.bv')
        // before calling .bootstrapValidator(options)
        .on('init.field.bv', function(e, data) {
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group'),
                $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]');

            // From v0.5.3, you can retrieve the icon element by
            // $icon = data.element.data('bv.icon');

            $icon.on('click.clearing', function() {
                // Check if the field is valid or not via the icon class
                if ($icon.hasClass('glyphicon-remove')) {
                    // Clear the field
                    data.bv.resetField(data.element);
                }
            });
        })
        .bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        }
                    }
                }
            }
        })
        .on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            saveCustomer();
        });

});


function saveProject(){
    var data = $('#newProjectForm').serialize();
    var selected = [];
    $("#projects  option:selected").each(function () {
        selected.push($(this).val());
    });

    if(selected.length > 0)
    {
        var dataProjects = {
            'projects' : selected
        };
        data += '&' + $.param(dataProjects);
    }

    $.ajax({
        type: 'POST',
        url: $('#newProjectForm').attr('action'),
        data: data,
        beforeSend: function(jqXHR, settings){
            //loadStart();
        },
        success: function(data, textStatus, jqXHR){
         //   var newHtml = "<li><a href='javascript:void(0);'><label class='checkbox'>";
         //       newHtml+="<input id='project-"+data.id+"'"+" type='checkbox' value='"+data.id+"'"+"> "+data.name+"</label></a></li>";
         //   $(".multiselect-container").append(newHtml);
        //    var htmlBtn = data.name+"&nbsp;<b class='caret'></b>";
        //    $(".multiselect").html(htmlBtn);
            loadProjects();
            setTimeout(function(){
                $('#newProjectModal').modal('hide');
                $("button.multiselect").trigger( "click" );
                $( "#project-"+data.id ).trigger( "click" );
                $('#newProjectForm').bootstrapValidator('resetForm', true);
                var msg = Translator.trans('The project has been added successfully');
                sendNotify('Info',msg, 'success');
            },1000);

        },
        error: function(jqXHR, textStatus, errorThrown){
            var msg = Translator.trans('Could not complete the action');
            sendNotify('Info',msg, 'error');
        },
        complete: function(jqXHR, textStatus){
            //loadStop();
        }
    });
}


$(document).ready(function() {

    $('#newProjectForm')
        // IMPORTANT: You must declare .on('init.field.bv')
        // before calling .bootstrapValidator(options)
        .on('init.field.bv', function(e, data) {
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group'),
                $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]');

            // From v0.5.3, you can retrieve the icon element by
            // $icon = data.element.data('bv.icon');

            $icon.on('click.clearing', function() {
                // Check if the field is valid or not via the icon class
                if ($icon.hasClass('glyphicon-remove')) {
                    // Clear the field
                    data.bv.resetField(data.element);
                }
            });
        })
        .bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        }
                    }
                }
            }
        })
        .on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            saveProject();
        });

});