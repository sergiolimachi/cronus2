function activeButton()
{
    $("#btnFilters").prop( "disabled", false );
}

function inactiveButton()
{
    $("#btnFilters").prop( "disabled", true );
}

function filterCustomer(id)
{
    inactiveButton();
    $('#c' + id).prop('selected', true);

    if(id == 0)
        $("#customers").prop( "disabled", true );
    else
        $("#customers").prop( "disabled", false );

    activeButton();
}


function filterProjects(projectsId)
{
    inactiveButton();
    if(projectsId == 0)
    {
        $('#p0').prop('selected', true);
        $("#projects").prop( "disabled", true );
    }
    else
    {
        $("#projects").prop( "disabled", false );
        loadProject(projectsId);
    }

    activeButton();
}


function loadProject(projectsId,projectId)
{
    var url = Routing.generate('project_with_customer_list');
    $.get( url, function( response ) {
        if(typeof response.empty === 'undefined')
        {
            var msg = Translator.trans('Select A Project');
            var html ="<select id='projects' name='project' class='form-control'>";
            if(projectId == 0)
                html+="<option selected='true' id='p0' value='0'>"+msg+"</option></select>";
            else
                html+="<option id='p0' value='0'>"+msg+"</option></select>";


            $("#projects_div").html(html);
            $.each(response.data, function(key, value) {
                if(typeof projectsId === 'undefined')
                {
                    if(typeof value.customer_id === 'undefined')
                    {
                        if(projectId == value.id)
                            var optionHtml = "<option selected='true' onclick='filterCustomer(0)'></option>";
                        else
                            var optionHtml = "<option onclick='filterCustomer(0)'></option>";

                    }
                    else
                    {
                        if(projectId == value.id)
                            var optionHtml = "<option selected='true' "+"onclick="+'filterCustomer('+value.customer_id+")></option>";
                        else
                            var optionHtml = "<option "+"onclick="+'filterCustomer('+value.customer_id+")></option>";
                    }

                    $('#projects')
                        .show()
                        .append($(optionHtml)
                            .attr("value",value.id)
                            .text(value.name));
                }else{
                    var valid = false;
                    var arr = (projectsId.split("-"));

                    $.each(arr, function(k, v) {
                        if(v == value.id)
                            valid = true;
                    });

                    if(valid == true )
                    {
                        if(typeof value.customer_id === 'undefined')
                        {
                            if(projectId == value.id)
                                var optionHtml = "<option selected='true' onclick='filterCustomer(0)'></option>";
                            else
                                var optionHtml = "<option onclick='filterCustomer(0)'></option>";
                        }
                        else
                        {
                            if(projectId == value.id)
                                var optionHtml = "<option selected='true'  "+"onclick="+'filterCustomer('+value.customer_id+")></option>";
                            else
                                var optionHtml = "<option "+"onclick="+'filterCustomer('+value.customer_id+")></option>";
                        }
                        $('#projects')
                            .show()
                            .append($(optionHtml)
                                .attr("value",value.id)
                                .text(value.name));
                    }
                }
            });
            $("#projects_div").show();
        }else{
            $('#projects').hide();
            $("#projects_div").show();

            var msg1 = Translator.trans('There are no projects');
            $("#projects_div").html("<p class='help-block'>"+msg1+"</p>");
        }
    });
}

function loadCustomer(customerId)
{
    //$("#customers_div").html('');
    var url = Routing.generate('customer_with_project_list');
    $.get( url, function( response ) {
        if(typeof response.empty === 'undefined')
        {
            var msg = Translator.trans('Select A Customer');
            var opFunction = "filterProjects(\'All\')";
            var html ="<select id='customers' name='customer' class='form-control'>";
            if(customerId == 0)
                html+="<option onclick='filterProjects()' selected='true' id='c0' value='0'>"+msg+"</option></select>";
            else
                html+="<option onclick='filterProjects()' id='c0' value='0'>"+msg+"</option></select>";

            //$("#customers_div").append(html);
            $("#customers_div").html(html);
            $.each(response.data, function(key, value) {
                if(typeof value.projects === 'undefined')
                {
                    if(customerId == value.id)
                        var optionHtml = "<option onclick='filterProjects(0)' selected='true' ></option>";
                    else
                        var optionHtml = "<option onclick='filterProjects(0)' ></option>";
                }
                else 
                {
                    var ps = "'"+value.projects+"'";

                    if(customerId == value.id)
                        var optionHtml = "<option selected='true' "+"onclick="+'filterProjects('+ps+")></option>";
                    else
                        var optionHtml = "<option "+"onclick="+'filterProjects('+ps+")></option>";
                }

                $('#customers')
                    .show()
                    .append($(optionHtml)
                        .attr("value",value.id)
                        .attr("id","c"+value.id)
                        .text(value.name));
            });
            $("#customers_div").show();
        }else{
            $('#customers').hide();
            $("#customers_div").show();

            var msg1 = Translator.trans('There are no customers');
            $("#customers_div").html("<p class='help-block'>"+msg1+"</p>");
        }
    });
}


function loadProjectsAndCustomer(customerId)
{
   // loadProject(undefined,projectId);
    loadCustomer(customerId);
}
