function ledOff()
{
    $("#led-on").hide();
    $("#led-off").show();
}

function ledOn()
{
    $("#led-off").hide();
    $("#led-on").show();
}

function truncateString(myString){
    var length = 12;
    var myTruncatedString = myString.substring(0,length)+"...";
    return myTruncatedString;
}

function selectFilterCustomer(customerId,obj,id)
{
    if($(obj).text().length > 15 )
        var text = truncateString($(obj).text());
    else
        var text = $(obj).text();

    $("#project_time").val(id);

    var html = text+" <span class='caret'></span>";
    $("#projectButton").html(html);

    if(customerId == 0)
    {
        //$( '#ct0 a' ).trigger( "click" );
        //$("#btnIdCustomer").prop( "disabled", true );
        var msg = Translator.trans('Without Customer');
        var html = msg + " <span class='caret'></span>";
        $("#customerButton").html(html);
        $("#customer_time").val(0);
    }
    else
    {
        //$( '#ct' + customerId +" a" ).trigger( "click" );
        //$("#btnIdCustomer").prop( "disabled", false );
        var msg = $( '#ct' + customerId +" a" ).text();
        var html = msg + " <span class='caret'></span>";
        $("#customerButton").html(html);
        $("#customer_time").val(customerId);
        $("#btnIdCustomer").prop( "disabled", false );
    }

    updateRunningEntry();
    return true;
}


function selectFilterProjects(projectsId,obj,id)
{
    var msg = Translator.trans('Without Project');
    var html = msg + " <span class='caret'></span>";
    $("#projectButton").html(html);
    $("#project_time").val(0);

    if ($(obj).text().length > 15)
        var text = truncateString($(obj).text());
    else
        var text = $(obj).text();

    $("#customer_time").val(id);

    var html = text + " <span class='caret'></span>";
    $("#customerButton").html(html);
    if (projectsId == '0') {
        loadProjects(undefined);
    }
    else {
        $("#btnIdProject").prop("disabled", false);
        console.info("carga projectos");
        loadProjects(projectsId);
    }

    updateRunningEntry();
    return true;

}


function loadProjects(projectsId,projectId)
{
    var url = Routing.generate('project_with_customer_list');
    $.get( url, function( response ) {
        if(typeof response.empty === 'undefined')
        {
            var msg = Translator.trans('Without Project');
            var html =" ";
            //if(projectId == 0)
            var ps = "'0',this,'0'";
                //html+="<li> <a href='#'>"+msg+"</a></li>";
            html+="<li id='pt0'><a href='#' "+"onclick="+'selectFilterCustomer('+ps+")>"+msg+"</a></li>";
            //else
            //    html+="<li> <a href='#'>"+msg+"</a></li>";
            html +=" <li class='divider'></li>";

            $("#projects_ ul").html(html);
            $.each(response.data, function(key, value) {
                if(typeof projectsId === 'undefined')
                {
                    if(typeof value.customer_id === 'undefined')
                    {
                        var ps = "0,this,"+value.id;
                        if(projectId == value.id)
                            var optionHtml = "<li><a href='#' "+"onclick="+'selectFilterCustomer('+ps+")>"+value.name+"</a></li>";
                        else
                            var optionHtml = "<li><a href='#' "+"onclick="+'selectFilterCustomer('+ps+")>"+value.name+"</a></li>";

                    }
                    else
                    {
                        var ps = "'"+value.customer_id+"',this,"+value.id;
                        if(projectId == value.id)
                            var optionHtml = "<li><a href='#' "+"onclick="+'selectFilterCustomer('+ps+")>"+value.name+"</a></li>";
                        else
                            var optionHtml = "<li><a href='#' "+"onclick="+'selectFilterCustomer('+ps+")>"+value.name+"</a></li>";
                    }

                    $('#projects_ ul')
                        .append($(optionHtml)
                            .attr("value",value.id)
                            .attr("id","pt"+value.id));
                            //.text(value.name));
                }else{
                    console.info("empieza a iterar por proyectos");
                    var valid = false;
                    var arr = (projectsId.split("-"));

                    $.each(arr, function(k, v) {
                        if(v == value.id)
                            valid = true;
                    });

                    if(valid == true )
                    {
                        if(typeof value.customer_id === 'undefined')
                        {
                            var ps = "'"+value.customer_id+"',this,"+value.id;
                            if(projectId == value.id)
                                var optionHtml = "<li><a href='#' "+"onclick="+'selectFilterCustomer('+ps+")>"+value.name+"</a></li>";
                            else
                                var optionHtml = "<li><a href='#' "+"onclick="+'selectFilterCustomer('+ps+")>"+value.name+"</a></li>"
                        }
                        else
                        {
                            var ps = "'"+value.customer_id+"',this,"+value.id;
                            if(projectId == value.id)
                                var optionHtml = "<li><a href='#' "+"onclick="+'selectFilterCustomer('+ps+")>"+value.name+"</a></li>";
                            else
                                var optionHtml = "<li><a href='#' "+"onclick="+'selectFilterCustomer('+ps+")>"+value.name+"</a></li>"
                        }
                        $('#projects_ ul')
                            .append($(optionHtml)
                                .attr("value",value.id)
                                .attr("id","pt"+value.id));
                               // .text(value.name));
                    }
                }
            });
            //$("#projects_div").show();
        }else{
            var msg1 = Translator.trans('There are no projects');
            $("#projects_div").html("<p class='help-block'>"+msg1+"</p>");
        }
    });
 
    return true;
}

function loadCustomers(customerId)
{
    //$("#customers_div").html('');
    var url = Routing.generate('customer_with_project_list');
    $.get( url, function( response ) {
        if(typeof response.empty === 'undefined')
        {
            var msg = Translator.trans('Without Customer');
            var html=" ";
           // if(customerId == 0)
            //{
            var ps = "'0',this,'0'";
            html += "<li id='ct0'><a href='#' "+"onclick="+'selectFilterProjects('+ps+")>"+msg+"</a></li>";
           // }
           // else
            //    html+="<li><a onclick='filterProjects()' id='ct0' value='0'>"+msg+"</a></li>";

            html +=" <li class='divider'></li>";

            //$("#customers_ ul").html(html);
            $.each(response.data, function(key, value) {
                if(typeof value.projects === 'undefined')
                {
                    var ps = "0,this,"+value.id;
                    if(customerId == value.id)
                        var optionHtml = "<li><a href='#' "+"onclick="+'selectFilterProjects('+ps+")>"+value.name+"</a></li>";
                    else
                        var optionHtml = "<li><a href='#' "+"onclick="+'selectFilterProjects('+ps+")>"+value.name+"</a></li>";
                }
                else
                {
                    var ps = "'"+value.projects+"',this,"+value.id;

                    if(customerId == value.id)
                        var optionHtml = "<li><a href='#' "+"onclick="+'selectFilterProjects('+ps+")>"+value.name+"</a></li>";
                    else
                        var optionHtml = "<li><a href='#' "+"onclick="+'selectFilterProjects('+ps+")>"+value.name+"</a></li>";
                }
                $('#customers_ ul')
                    .append($(optionHtml)
                        .attr("id","ct"+value.id));
                        //.text(value.name));
            });
            //$("#customers_ul").show();
        }else{
            var msg1 = Translator.trans('There are no customers');
            $("#customers_div").html("<p class='help-block'>"+msg1+"</p>");
        }
    });

    return true;
}


function loadProjectsCustomer(projectId,customerId)
{
    loadProjects(undefined,projectId);
    loadCustomers(customerId);
}


function startTimeEntry(){

    ledOn();
    secs = 0;
    timer = setInterval(function(){
        secs += 1;
        var ss = secs % 60;
        var mm = ((secs - ss) / 60) % 60;
        var hh = ((secs - ss - (mm * 60)) / 3600) % 24;
        $('#timer').html(padLeft(hh) + ':' + padLeft(mm) + ':' + padLeft(ss));
    }, 1000);
    
    var startTime = moment(new Date()).format('DD-MM-YYYY HH:mm:ss');
    $('#newTimeEntryForm #start').val(startTime);
    $.ajax({
        type: 'POST',
        url: $('#startTimeEntry').data('url'),
        data: $('#newTimeEntryForm').serialize(),
        beforeSend: function(jqXHR, settings){
           // loadStart();
        },
        success: function(data, textStatus, jqXHR){
            $('#newTimeEntryForm #id').val(data.id);
            if(data.description){
                $('#newTimeEntryForm #description').val(data.description);
            }

            $('#startTimeEntry').hide();
            $('#stopTimeEntry').show();
            $('#discardTimeEntry').show();
        },
        error: function(jqXHR, textStatus, errorThrown){
        },
        complete: function(jqXHR, textStatus){
             // loadStop();
        }
    });
}

function stopTimeEntry(){
    ledOff();
    $('#startTimeEntry').show();
    $('#stopTimeEntry').hide();
    $('#discardTimeEntry').hide();
    var stopTime = moment(new Date()).format('DD-MM-YYYY HH:mm:ss');
    $.ajax({
        type: 'POST',
        url: $('#stopTimeEntry').data('url'),
        //data: 'id=' + $('#newTimeEntryForm #id').val() + '&time=' + stopTime.toLocaleString(),
        data: $('#newTimeEntryForm').serialize() + '&time=' + stopTime,
        beforeSend: function(jqXHR, settings){
            //loadStart(); 
        },
        success: function(data, textStatus, jqXHR){
            $("#id").val("");
            $('#newTimeEntryForm')[0].reset();
            $("#description").val("");
            $("#project_time").val("");
            $("#customer_time").val("");
            clearInterval(timer);
            $('#timer').html('00:00:00');
            $('#tags_div_ input').tagsinput('destroy');
            $("#tags_div_ input").tagsinput('removeAll');
            loadTimeEntryList();
            updateProjectCustomer(0, 'Without Project', 0);
            var msg = Translator.trans('The time entry has been saved successfully');
            sendNotify('Info',msg, 'success');
            $('#startTimeEntry').show();
            $('#stopTimeEntry').hide();
            $('#discardTimeEntry').hide();
        },
        error: function(jqXHR, textStatus, errorThrown){
            var msg = Translator.trans('Could not complete the action');
           // sendNotify('Error', msg, 'error');
        },
        complete: function(jqXHR, textStatus){
            //loadStop();
        }
    });
}

function updateProjectCustomer(id, name, customerId)
{ 
    if(name.length > 15 )
        var text = truncateString(name);
    else
        var text = name;

    $("#project_time").val(id);

    var html = text+" <span class='caret'></span>";
    $("#projectButton").html(html);


    if(customerId == 0)
    {
        //$( '#ct0 a' ).trigger( "click" );
        //$("#btnIdCustomer").prop( "disabled", true );
        var msg = Translator.trans('Without Customer');
        var html = msg + " <span class='caret'></span>";
        $("#customerButton").html(html);
        $("#customer_time").val(0);
    }
    else
    {
        //$( '#ct' + customerId +" a" ).trigger( "click" );
        //$("#btnIdCustomer").prop( "disabled", false );
        var msg = $( '#ct' + customerId +" a" ).text();
        var html = msg + " <span class='caret'></span>";
        $("#customerButton").html(html);
        $("#customer_time").val(customerId);
        $("#btnIdCustomer").prop( "disabled", false );
    }

    updateRunningEntry();
    return true;
}

function continueTimeEntry(id, projectId, projectName, customerId){

    stopTimeEntry(); 
    $('#newTimeEntryForm #id').val(id);
    if(projectId > 0)
    {
        updateProjectCustomer(projectId, projectName, customerId); 
    }
    setTimeout(function(){
        startTimeEntry();
        console.info('again');
    }, 200);
}


function loadRunningEntry(){
    loadProjectsCustomer(0,0);
    var now = new Date();
    $.ajax({
        type: 'POST',
        url: $('#newTimeEntryForm').attr('action'),
        data: 'now='+now.toLocaleString(),
        beforeSend: function(jqXHR, settings){
            //loadStart();
        },
        success: function(data, textStatus, jqXHR){
            if(data.status === 'ok'){
                $('#newTimeEntryForm #description').val(data.description);
                $('#newTimeEntryForm #start').val(data.start);
                $('#newTimeEntryForm #id').val(data.id);

                clearInterval(timer);
                $('#timer').html(data.start.substr(11,8));

                $('#startTimeEntry').hide();
                $('#stopTimeEntry').show();
                $('#discardTimeEntry').show();
                secs = data.rt; 
                timer = setInterval(function(){
                    secs += 1;
                    var ss = secs % 60;
                    var mm = ((secs - ss) / 60) % 60;
                    var hh = ((secs - ss - (mm * 60)) / 3600) % 24;
                    $('#timer').html(padLeft(hh) + ':' + padLeft(mm) + ':' + padLeft(ss));
                }, 1000);
                ledOn();
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert(textStatus + ': ' + errorThrown);
        },
        complete: function(jqXHR, textStatus){
            //loadStop();
        }
    });
}
 
function updateRunningEntssssry(action_path){
    var id = $('#newTimeEntryForm #id').val();
    if(id > 0)
    {
        console.info("formulario", $('#newTimeEntryForm').serialize());
        if(typeof action_path === 'undefined')
        {
            var action_path = Routing.generate('runningentry_update');
        }
        $.post( action_path,$('#newTimeEntryForm').serialize(), function() {
            console.info( "success",action_path );
        }).done(function() {
            $('#newTimeEntryForm #description').val(description);
        });
    }else
    {
        return true;
    }
}

$( ".bootstrap-tagsinput" ).focusout(function() {
    console.info("aqui",$('#newTimeEntryForm').serialize());
});


function showTimeEntry(description, start, stop, tags, billable){
    var timeEntryModal = $('#timeEntryModal');
    timeEntryModal.find('#description_time').val(description);
    timeEntryModal.find('#start').val(start);
    timeEntryModal.find('#stop').val(stop);
    timeEntryModal.find('#tags').val(tags);
    timeEntryModal.find('#billable').prop('checked', billable);
    timeEntryModal.find('input,select').prop('disabled',true);
    timeEntryModal.find('#saveTimeEntry').hide();
    timeEntryModal.modal('show');
}

function discardTimeEntry(){
    $.ajax({
        type: 'GET',
        url: $(this).data('url').replace(/@id/, $('#newTimeEntryForm #id').val()),
        beforeSend: function(jqXHR, settings){
            loadStart();
        },
        success: function(data, textStatus, jqXHR){
            ledOff();
            $('#startTimeEntry').show();
            $('#stopTimeEntry').hide();
            $('#discardTimeEntry').hide();
    
            $('#newTimeEntryForm')[0].reset();
            clearInterval(timer);
            $('#timer').html('00:00:00');
            
            loadTimeEntryList();
        },
        error: function(jqXHR, textStatus, errorThrown){
            var msg = Translator.trans('Could not complete the action');
            sendNotify('Info',msg, 'error');
        },
        complete: function(jqXHR, textStatus){
            loadStop();
        }
    });
}

function loadAutocompleteWithTasks(value,that){
    var urldir = Routing.generate('ajax_search');
    var searchRequest = null;
    var minlength = 3;
    var html=" ";
    html +=" <li class='divider'></li>";
    $("#descriptiontask_ ul").html(html); 
    if (value.length >= minlength ) {
        if (searchRequest != null)
            searchRequest.abort();
            $.ajax({
            type: "GET",
            url: urldir,
            data: {
                'q' : value
            },
            dataType: "text",
            success: function(msg){
                if (value==$(that).val()) {
                    var result = JSON.parse(msg);
                    $.each(result, function(key, arr) {
                        $.each(arr, function(id, value) {
                            var myString = value;
                            var value3 = myString.substring(0,135);
                            var optionHtml ='<li><a href="#" onClick="insertDescription(\'' + value + '\')" >'+value3+'</a></li>';
                            $('#descriptiontask_ ul').append($(optionHtml).attr("id","ct"+id));
                        });
                    });
                }
            }
        });
    }
}

function insertDescription(value){
    $("#description").val(value);
}