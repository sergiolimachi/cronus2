function selectTimeEntry(that){
    $(that).toggleClass('active')
        .find('td:first input:checkbox[id^=id_]')
        .prop('checked', $(that).hasClass('active'));
    //var disabled = $('table.records_list > tbody > tr > td > input:checkbox[id^=id_]:checked').length === 0;
    //$('#deleteSelectedTimeEntries').toggleClass('disabled', disabled);
}
 

function selectAllTimeEntries(){
    $('table.records_list > tbody > tr > td > input:checkbox[id^=id_]').each(function(){
        this.checked = $('#checkAll').prop('checked');
    });
    //$('#deleteSelectedTimeEntries').toggleClass('disabled', !$('#checkAll').is(':checked') );
}

function loadTimeEntryList(){ 
    loadStart();
    self.week = 0; 
    $('#list').load($('#list').data('url'), function(){
        $('.delete_action').click(deleteTimeEntry);
        loadStop();
    });
}


function padLeft(value){
    return (value > 9 ? value : '0' + value);
}

function loadLastWeek(){
    loadStart();
    if(typeof self.week == "undefined")
    {
        self.week = 1;
    }
    else
        self.week = self.week + 1;

    $.ajax({
        url: Routing.generate('timeentry_list')+'?week='+self.week,
        async: true,
        datatype: 'json',
        success: function (data) {
            var tmp = $();
            tmp = tmp.add( $( data ) );
            $('#list').append(tmp);
            loadStop();
        },
        error: function (data, error, msg) {
            loadStop();
            $( "#moreList" ).append("<h4>No se ha podido realizar esta operación</h4>" );
            console.log('No se ha podido realizar esta operación');
        }
    });
}

function loadStart(){
    $.blockUI({css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    }});
}


function loadStop(){
    $.unblockUI();
}