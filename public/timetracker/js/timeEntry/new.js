var htmlProject;
var htmlCustomer;  
var description;
 
$(document).ready(function() {
    window.htmlProject =  $("#projects_div").html();
    window.htmlCustomer =  $("#customers_div").html();
    window.description =  $("#description").html();
    var timeEntryModal = $('#timeEntryModal');
    timeEntryModal.find('#description').keyup(function() {
        var that = this;
        var value = $(this).val();
        loadAutocompleteWithTasksModal(value,that);
    });
});

function activeButton()
{
    $("#btnSubmitTimeEntry").prop( "disabled", false );
}

function inactiveButton()
{
    $("#btnSubmitTimeEntry").prop( "disabled", true );
}

function filterCustomer(id)
{
    inactiveButton();
    $('#c' + id).prop('selected', true);

    if(id == 0)
        $("#customers").prop( "disabled", true );
    else
        $("#customers").prop( "disabled", false );

    activeButton();
}


function filterProjects(projectsId)
{

    inactiveButton();
    if(projectsId == 0)
    {
        $('#p0').prop('selected', true);
        $("#projects").prop( "disabled", true );
    }
    else
    {
        $("#projects").prop( "disabled", false );
        loadProject(projectsId);
    }

    activeButton();
}
 

function loadProject(projectsId,projectId)
{
    var url = Routing.generate('project_with_customer_list');
    $.get( url, function( response ) {
        if(typeof response.empty === 'undefined')
        {
            var msg = Translator.trans('Select A Project');
            var html ="<select id='projects' name='project' class='form-control' onchange='filterCustomer(this.value)' >";
            if(projectId == 0)
                html+="<option selected='true' id='p0' value='0'>"+msg+"</option></select>";
            else
                html+="<option id='p0' value='0'>"+msg+"</option></select>";


            $("#projects_div").html(html);
            $.each(response.data, function(key, val) {
                if(typeof projectsId === 'undefined')
                {
                    if(typeof val.customer_id === 'undefined')
                    {
                        if(projectId == val.id)
                            var optionHtml = "<option selected='true' value='0'></option>";
                        else
                            var optionHtml = "<option value='0'></option>";

                    }
                    else
                    {
                        if(projectId == val.id)
                            var optionHtml = "<option selected='true' value="+val.customer_id+")></option>";
                        else
                            var optionHtml = "<option value="+val.customer_id+")></option>";
                    }

                    $('#projects')
                        .show()
                        .append($(optionHtml)
                            .attr("value",val.id)
                            .text(val.name));
                }else{
                    var valid = false;
                    var arr = (projectsId.split("-"));

                    $.each(arr, function(k, v) {
                        if(v == val.id)
                            valid = true;
                    });

                    if(valid == true )
                    {
                        if(typeof val.customer_id === 'undefined')
                        {
                            if(projectId == val.id)
                                var optionHtml = "<option selected='true' value='0'></option>";
                            else
                                var optionHtml = "<option value='0'></option>";
                        }
                        else
                        {
                            if(projectId == val.id)
                                var optionHtml = "<option selected='true' value="+val.customer_id+"></option>";
                            else
                                var optionHtml = "<option value="+val.customer_id+"></option>";
                        }
                        $('#projects')
                            .show()
                            .append($(optionHtml)
                                .attr("value",val.id)
                                .text(val.name));
                    }
                }
            });
            $("#projects_div").show();
        }else{
            $('#projects').hide();
            $("#projects_div").show();
        }
    });
}

function loadCustomer(customerId)
{
    $("#customers_div").html('');
    var url = Routing.generate('customer_with_project_list');
    $.get(url, function( response ) {
        if(typeof response.empty === 'undefined')
        {
            var msg = Translator.trans('Select A Customer');
            var opFunction = "filterProjects(\'All\')";
            var html ="<select id='customers' name='customer' class='form-control'>";
            if(customerId == 0)
                html+="<option onclick='filterProjects()' selected='true' id='c0' value='0'>"+msg+"</option></select>";
            else
             html+="<option onclick='filterProjects()' id='c0' value='0'>"+msg+"</option></select>";
            $("#customers_div").html(html);
            $.each(response.data, function(key, val) {
                if(typeof val.projects === 'undefined')
                {
                    if(customerId == val.id)
                        var optionHtml = "<option value='0' selected='true' ></option>";
                    else
                        var optionHtml = "<option value='0' ></option>";
                }
                else
                {
                    var ps = "'"+val.projects+"'";

                    if(customerId == val.id)
                        var optionHtml = "<option selected='true' value='"+ps+"'></option>";
                    else
                        var optionHtml = "<option value='"+ps+"'></option>";
                }

                $('#customers')
                    .show()
                    .append($(optionHtml)
                        .attr("value",val.id)
                        .attr("id","c"+val.id)
                        .text(val.name));
            });
            $("#customers_div").show();
        }else{
            $('#customers').hide();
            $("#customers_div").show();
        }
    });
}


function loadProjectsAndCustomer(projectId,customerId)
{
    loadProject(undefined,projectId);
    loadCustomer(customerId);
    

}

function newTimeEntry(){
    loadProjectsAndCustomer(0,0);
    var timeEntryModal = $('#timeEntryModal');
    timeEntryModal.find('#id').val('');
    timeEntryModal.find('#description').val('');
    timeEntryModal.find('#start').val('');
    timeEntryModal.find('#stop').val('');
    timeEntryModal.find('#tags').val('');
    $('#tags_div input').tagsinput('destroy');
    $('#billable').removeAttr("checked");
    timeEntryModal.find('input,select').removeProp('disabled');
    timeEntryModal.find('#saveTimeEntry').show();
    timeEntryModal.modal('show');
}


function saveTimeEntry(){
    $.ajax({
        type: 'POST',
        url: $('#timeEntryForm').attr('action'),
        data: $('#timeEntryForm').serialize(),
        beforeSend: function(jqXHR, settings){
            //loadStart();
        },
        success: function(data, textStatus, jqXHR){
            $('#timeEntryModal').modal('hide');
            loadTimeEntryList();
            $('#timeEntryForm').bootstrapValidator('resetForm', true);
            $("#tags").val('');
            var msg = Translator.trans('The time entry has been added successfully');
            if($('#id').val() > 0 )
            {
                msg = Translator.trans('The time entry has been updated successfully');
            }

            $("#customers_div").html(window.htmlCustomer);
            $("#projects_div").html(window.htmlProject);


            sendNotify('Info',msg, 'success');
        },
        error: function(jqXHR, textStatus, errorThrown){
            var msg = Translator.trans('Could not complete the action');
            sendNotify('Info',msg, 'error');
        },
        complete: function(jqXHR, textStatus){
            //loadStop();
        }
    });
}

$(document).ready(function() {

    $('#timeEntryForm')
        // IMPORTANT: You must declare .on('init.field.bv')
        // before calling .bootstrapValidator(options)
        .on('init.field.bv', function(e, data) {
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group'),
                $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]');

            // From v0.5.3, you can retrieve the icon element by
            // $icon = data.element.data('bv.icon');

            $icon.on('click.clearing', function() {
                // Check if the field is valid or not via the icon class
                if ($icon.hasClass('glyphicon-remove')) {
                    // Clear the field
                    data.bv.resetField(data.element);
                }
            });
        })
        .bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                start: {
                    validators: {
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        },
                        date: {
                            format: 'DD-MM-YYYY H:m:s',
                            message: Translator.trans('Invalid Date')
                        }
                    }
                },
                stop: {
                    validators: {
                        notEmpty: {
                            message: Translator.trans('This field is required')
                        },
                        date: {
                            format: 'DD-MM-YYYY H:m:s',
                            message: Translator.trans('Invalid Date')
                        }
                    }
                }
            }
        })
        .on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            saveTimeEntry();
        });

});
 
function loadAutocompleteWithTasksModal(txtsearch,that){
    var searchRequest = null;
    var minlength = 1;
    if (txtsearch.length >= minlength ) {
        if (searchRequest != null)
            searchRequest.abort();
            var html=" ";
            html +=" <li class='divider'></li>";
            $("#tasks_div ul").html(html);
            var url = Routing.generate('search_task',{txtsearch});
            $.get(url, function( msg ) {
                if (txtsearch==$(that).val()) {
                    var result = JSON.parse(msg);
                    $.each(result, function(key, arr) {
                        $.each(arr, function(id, value) {
                            var myString = value;
                            var value2 = myString.substring(0,85);
                            var optionHtml ='<li><a type="text"  onClick="insertDescriptionModal(\'' + value + '\')" >'+value2+'</a></li>';
                            $('#tasks_div ul').append($(optionHtml).attr("id","c"+id));
                        }); 
                    });
                }
            });
    }
}
 
function insertDescriptionModal(value){
    var timeEntryModal = $('#timeEntryModal');
    timeEntryModal.find('#description').val(value);
}
