function editTimeEntry(id, description, start, stop, tags, billable,project_id,customer_id){
    loadProjectsAndCustomer(project_id,customer_id);
    var timeEntryModal = $('#timeEntryModal');
    timeEntryModal.find('#id').val(id);
    timeEntryModal.find('#description').val(description);
    timeEntryModal.find('#start').val(start);
    timeEntryModal.find('#stop').val(stop);
    timeEntryModal.find('#tags').val(tags);
    $('#tags_div #tags').tagsinput('destroy');

    $('#startDateTimePicker').data("DateTimePicker").date(start);
    $('#stopDateTimePicker').data("DateTimePicker").date(stop);

    var arr = (tags.split("-"));

    $.each(arr, function(k, v) {
        $('#tags_div #tags').tagsinput('add',v);
    });

    if(billable == 1)
        $('#billable').attr("checked",'true');
    else
        $('#billable').removeAttr("checked");
    timeEntryModal.find('input,select').removeProp('disabled');
    timeEntryModal.find('#saveTimeEntry').show();
    timeEntryModal.modal('show');
}
