function deleteTimeEntry(url)
{
    $.get( url, function( data ) {
        $('#confirm-delete').modal('hide');
        loadTimeEntryList();
        var msg = Translator.trans('The time entry has been deleted successfully');
        sendNotify('Info',msg, 'success');
    });
}




function deleteSelectedTimeEntries(){
    if($('table.records_list > tbody > tr > td > input:checkbox[id^=id_]:checked').length === 0) return false;
    var msg = Translator.trans('Do you want to delete the selected entries?');
    if(!confirm(msg)){
        return false;
    }
    var ids = [];
    $('table.records_list > tbody > tr > td > input:checkbox:checked').each(function(){
        ids.push($(this).val());
    });
    $.ajax({
        url: $('#deleteSelectedTimeEntries').data('url').replace(/@ids/, ids),
        type: 'POST',
        beforeSend: function(){
            loadStart();
        },
        success: function(data){
            //var msg = data + '<br>Please wait while the page is reloaded to see the changes.';
            //$('#result').removeClass('alert-danger').addClass('alert-success').find('#message').html(msg).end().fadeIn(250);//.delay(5000).fadeOut(400);
            loadTimeEntryList();
        },
        error: function(jqXHR){
            //var msg = jqXHR.responseText + '<br>Please reload the page manually to see the changes.';
            //$('#result').removeClass('alert-success').addClass('alert-danger').find('#message').html(msg).end().fadeIn(250);//.delay(5000).fadeOut(400);
        },
        complete: function(){
            loadStop();
        }
    });
}