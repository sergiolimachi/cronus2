<?php

namespace App\Command;
use App\Controller\ConsumeapijiraController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class MigrateTasksCommand extends Command
{
    protected static $defaultName = 'app:migratetask';
    public function __construct(ConsumeapijiraController $consumeapijiraController)
    {
        $this->consumeapijiraController = $consumeapijiraController;
        parent::__construct();
    }

    protected function configure()
    {
      $this
       ->setDescription('Parámetro')
       ->setHelp('Este comando saluda.')
       ->addArgument('name', InputArgument::REQUIRED, 'Pass the name.')
       ->addArgument('token', InputArgument::REQUIRED, 'Pass the token.')
       ->addArgument('dias', InputArgument::REQUIRED, 'Pass the day.');
       
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
      $name = $input->getArgument('name');
      if (empty($name)) {
            throw new \Exception('Missing parameter name');
            return Command::FAILURE;
      }
      $token = $input->getArgument('token');
      if (empty($token)) {
            throw new \Exception('Missing parameter token');
            return Command::FAILURE;
      }
      $dias = $input->getArgument('dias');
 
      $result = $this->consumeapijiraController->listMigrate($name,$token,$dias);
      if($result==true){
        $output->writeln('Process completed successfully URLname:'.$name.' Token:'.$token.' Dias:'.$dias);
        return Command::SUCCESS;
      }else{
        throw new \Exception('Failed process ');
        return Command::FAILURE;
      }

    }
    //php bin/console app:migratetask 'pruebagre1' , 'c2VyZ2lvbGltYWNoaTdAZ21haWwuY29tOlgxZXdFWHJOaXB1dzhSME1JQ1pZRjE5OA==' , 30
        //php bin/console app:migratetask 'pruebagre1' , 'c2VyZ2lvbGltYWNoaTdAZ21haWwuY29tOlgxZXdFWHJOaXB1dzhSME1JQ1pZRjE5OA==' , 10

}
