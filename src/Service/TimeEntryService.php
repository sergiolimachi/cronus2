<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service;

use Doctrine\ORM\EntityManager;
use App\Entity\TimeEntry;
use App\Entity\Customer;
use App\Entity\Project;
/**
 * Description of TimeEntryService
 *
 * @author xmsim
 */
class TimeEntryService {

    protected $user;
    protected $em;

    public function __construct($user, EntityManager $em) {
        $this->user = $user;
        $this->em = $em;
    }

    public function save($entity) {
        if ($entity->getId() == null) {
            $this->em->persist($entity);
        } else {
            $this->em->merge($entity);
        }
        $this->em->flush();
        return $entity->getId();
    }

    public function delete($id) {
        $entity = $this->em->getRepository(TimeEntry::Class)->find($id);
        $this->em->remove($entity);
        $this->em->flush();
    }

    public function start($description, $start, $tags, $billable, $project, $customer) {
        $entity = new TimeEntry();
        $entity->setDescription($description);
        $entity->setStart($start instanceof \DateTime ? $start : new \DateTime($start) );
        $entity->setStop(null);
        $entity->setTags($tags);
        $entity->setCreated(new \DateTime());
        $entity->setModified(new \DateTime());
        $entity->setDeletedAt(null);
        $entity->setBillable($billable ? $billable : false);
        $entity->setUser($this->user);
 
        if($customer)
        {
            $entityCustomer= $this->em->getRepository(Customer::Class)->findOneBy(array('id'=> $customer));
            $entity->setCustomer($entityCustomer);
        }

        if($project)
        {
            $entityProject = $this->em->getRepository(Project::Class)->findOneBy(array('id'=> $project));
            $entity->setProject($entityProject);
        }

        return $this->save($entity);
    }

    public function stop($id, $time,$description, $tags, $project, $customer) {
        $entity = $this->em->getRepository(TimeEntry::Class)->find($id);
        $entity->setDescription($description);
        $entity->setTags($tags);

        if($customer)
        {
            $entityCustomer= $this->em->getRepository(Customer::Class)->findOneBy(array('id'=> $customer));
            $entity->setCustomer($entityCustomer);
        }

        if($project)
        {
            $entityProject = $this->em->getRepository(Project::Class)->findOneBy(array('id'=> $project));
            $entity->setProject($entityProject);
        }
        $entity->setStop($time instanceof \DateTime ? $time : new \DateTime($time));
        return $this->save($entity);
    }

    public function getRunningEntry() {
        $entity = $this->em->getRepository(TimeEntry::Class)->findOneBy(array('user' => $this->user,'deletedAt'=> NULL,'stop' => NULL));
        return $entity;
    }

    public function deleteBatch($ids) {
        $ids = explode(',', $ids);

        $qb = $this->em->createQueryBuilder();
        $qb->delete(TimeEntry::Class, 'e');
        $qb->where($qb->expr()->in('e.id', ':ids'));
        $qb->setParameter(':ids', $ids);
        $qb->getQuery()->execute();

        return true;
    }

}
