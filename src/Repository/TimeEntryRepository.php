<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;


class TimeEntryRepository extends EntityRepository
{

    public function getCurrentWeek()
    {
        $start = date("Y-m-d H:i:00", strtotime("Monday this week"));
        $finish = date("Y-m-d H:i:00", strtotime("Sunday this week"));

        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('t')
            ->from('TimeEntry', 't')
            ->where('t.start > :start')
            ->andWhere('t.start < :finish')
            ->setParameter('start', $start)
            ->setParameter('finish', $finish);

        return $qb->getQuery()->getResult();
    }



    public function getWeekByUser($userId, $start, $finish){

        $sql = "SELECT timeentry.*, TIME_FORMAT(TIMEDIFF(stop,start), '%k:%i:%s') as totalTime, "
            . "project.name AS project_name, customer.name AS customer_name, "
            . "project.id AS project_id, customer.id AS customer_id "
            . "FROM timeentry "
            . "LEFT JOIN customer ON timeentry.customer = customer.id "
            . "LEFT JOIN project ON timeentry.project = project.id "
            . "WHERE stop IS NOT NULL AND timeentry.deletedAt IS NULL  AND "
            . " (timeentry.start > '".$start."' AND timeentry.start < '".$finish."') AND "
            . "timeentry.user = ".$userId." "
            . "ORDER BY start DESC;";


        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

}