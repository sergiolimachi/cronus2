<?php

namespace App\Repository;

use App\Entity\TaskJira;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskJiraRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskJira::class);
    }

    public function findTaskByString($str){
        return $this->getEntityManager()
            ->createQuery(
                'SELECT e
                FROM App:TaskJira e
                WHERE e.descripcion_task LIKE :str'
            )
            ->setParameter('str', '%'.$str.'%')
            ->getResult();
    }


}
