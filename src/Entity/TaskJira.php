<?php

namespace App\Entity;
use App\Repository\TaskJiraRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="taskjira")
 * @ORM\Entity(repositoryClass=TaskJiraRepository::class)
 */
class TaskJira
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="id_codigo_project", type="string")
     */
    protected $id_codigo_project;

    /**
     * @ORM\Column(name="codigo_task", type="string", length=255)
     */
    protected $codigo_task;

    /**
     * @ORM\Column(name="descripcion_task", type="string", length=500)
     */
    protected $descripcion_task;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdCodigoProject(): ?string
    {
        return $this->id_codigo_project;
    }

    public function setIdCodigoProject(string $id_codigo_project): self
    {
        $this->id_codigo_project = $id_codigo_project;

        return $this;
    }

    public function getCodigoTask(): ?string
    {
        return $this->codigo_task;
    }

    public function setCodigoTask(string $codigo_task): self
    {
        $this->codigo_task = $codigo_task;

        return $this;
    }

    public function getDescripcionTask(): ?string
    {
        return $this->descripcion_task;
    }

    public function setDescripcionTask(string $descripcion_task): self
    {
        $this->descripcion_task = $descripcion_task;

        return $this;
    }




}