<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Customer;
use App\Entity\Project;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;
/**
 * Project controller.
 * @Route("/project")
 */
class ProjectController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/", name="project")
     * @Template("grxtimetracker/project/index.html.twig")
     */
    public function index()
    {
        if ($this->security->isGranted('ROLE_SUPER_ADMIN')) {
            return array();
        }else{
            return $this->redirectToRoute('timeentry');
        }
    }
    
    /**
     * Lists all Customer entities.
     * @Route("/list/", name="project_list")
     * @Method("GET")
     * @Template("grxtimetracker/project/list.html.twig")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository(Project::class)->findAll();  
        return array(
            'entities' => $entities,
        );
    }

   /**
     * List all project of user
     * @Route("/list/without_customer/{customerId}", name="project_without_customer_list")
     * @Method("GET")
     * @return Object JsonResponse
     */
    public function listWithoutCustomerAction($customerId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository(Project::Class)->findBy(array('customer' => null ));

        if($entities)
        {
            $responseData =  array();
            $cont = 0;
            foreach($entities as $item)
            {
                $responseData[$cont]['id'] = $item->getId();
                $responseData[$cont]['name'] = $item->getName();
                $cont++;
            }
            if($customerId){
                $entitiesC = $em->getRepository(Project::Class)->findBy(array('customer' => $customerId ));
                if($entitiesC)
                {
                    foreach($entitiesC as $item)
                    {
                        $responseData[$cont]['id'] = $item->getId();
                        $responseData[$cont]['name'] = $item->getName();
                        $responseData[$cont]['selected'] = 'selected';
                        $cont++;
                    }
                }
            }
            return new JsonResponse(array('data' => $responseData));
        }else
            return new JsonResponse(array('empty' => true));
    }

    /**
     * Deletes a Project entity.
     *
     * @Route("/delete/{id}/", name="project_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em =  $this->getDoctrine()->getManager();
        $entity = $em->getRepository(Project::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        $em->remove($entity);
        $em->flush();
        return new JsonResponse(array('status'=> true));
    }

    /**
     * @Route("/save", name="project_save")
     * @Method("POST")
     */
    public function saveAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = null;
        $id = $request->get('id');
        if($id){
            $entity = $em->getRepository(Project::class)->find($id);
        }
        if(!$entity) {
            $entity = new Project();
            /* $user = $this->getUser();
            $entity->setUser($user); project_list  project_save   */
        }
        $entity->setName($request->get('name'));
        $entity->setCreated(new \DateTime());
        $entity->setModified(new \DateTime());
        $customer = $request->get('customer');///corregir -----------------------------------------------------------------
        if($customer > 0)
        {
            $entityCustomer = $em->getRepository(Customer::class)->findOneBy(array('id'=> $customer));
            $entity->setCustomer($entityCustomer);
        }
        $em->persist($entity);
        $em->flush();

        return new JsonResponse(array('status'=> true, 'id' => $entity->getId(), 'name'=> $entity->getName()));
    }
    
    /**
    * List all project of user
    * @Route("/list/with_customer/", name="project_with_customer_list")
    * @Method("GET")
    * @return Object JsonResponse
    */
    public function listWithCustomerAction()
    {
        $em = $this->getDoctrine()->getManager();
        /* $user = $this->getUser(); */
        /* $userId = $user->getId(); */
        $entities = $em->getRepository(Project::Class)->getAllProjectOrderBy();
        if($entities)
        {
            $responseData =  array();
            $cont = 0;
            foreach($entities as $item)
            {
                $responseData[$cont]['id'] = $item->getId();
                $responseData[$cont]['name'] = $item->getName();
                if($item->getCustomer())
                {
                    $responseData[$cont]['customer_id'] = $item->getCustomer()->getId();
                }
                $cont++;
            }
            return new JsonResponse(array('data' => $responseData));
        }else
            return new JsonResponse(array('empty' => true));

    }



}
