<?php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Customer;
use App\Entity\Project;
use App\Entity\TaskJira;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpClient\HttpClient;

class ConsumeapijiraController extends AbstractController
{
    public function listMigrate($nameMaster,$api_token,$dias)
    {       $resp = false;
            $em = $this->getDoctrine()->getManager(); 
            $httpClient = HttpClient::create();
            $response = $httpClient->request('GET','https://'.$nameMaster.'.atlassian.net/rest/api/3/search?jql=created>=-'.$dias.'d&maxResults=200',
                                            [
                                                'headers' => [
                                                    'Accept' => 'application/json',
                                                    'Authorization' => 'Basic '.$api_token,
                                                ],
                                            ],                                      
                                        );
            if (200 !== $response->getStatusCode()) {
                $content ="No se pudo cargar las tareas";
            }else{
                $content = $response->getContent();
                $items = json_decode($content, true); 
                $listSuices = $items['issues'];
                $responseData =  array();
                $nameMasterAnterior ='';
                $em = $this->getDoctrine()->getManager();
                foreach($listSuices as $item){
                    $id = $item['id'];
                    $entity = $em->getRepository(TaskJira::class)->findOneBy(array('codigo_task' => $id));
                    if(!$entity) {
                        $entity = new TaskJira();
                        $entity->setIdCodigoProject($item['fields']['project']['key']);
                        $entity->setCodigoTask($item['id']);
                        $entity->setDescripcionTask($item['key'].' '.$item['fields']['summary']);
                        $em->persist($entity);
                        $em->flush();
                    }
                }
                $resp = true; 
            }
        return $resp;
    }

   /**
   * Creates a new ActionItem entity.
   * @Route("/search", name="ajax_search")
   * @Method("GET")
   */
  public function searchAction(Request $request)
  {
      $em = $this->getDoctrine()->getManager();
      $requestString = $request->get('q');
      $entities =  $em->getRepository(TaskJira::Class)->findTaskByString($requestString);
      if(!$entities) {
          $result['entities']['error'] = "Description not found";
      } else {
          $result['entities'] = $this->getRealEntities($entities);
      }
      return new Response(json_encode($result));
  }

  public function getRealEntities($entities){
    $realEntities;
      foreach ($entities as $entity){
          $realEntities[$entity->getId()] = $entity->getDescripcionTask();
      }
      return $realEntities;
  }

     /**
     * @Route("/search_tasks/{txtsearch}", name="search_task")
     * @Method("GET")
     * @return Object JsonResponse
     */
    public function search_tasksjira($txtsearch)
    {
        $em = $this->getDoctrine()->getManager();
        //$requestString = $request->get('q');
        $entities =  $em->getRepository(TaskJira::Class)->findTaskByString($txtsearch);
        if(!$entities) {
            $result['entities']['error'] = "Description not found";
        } else {
            $result['entities'] = $this->getRealEntities($entities);
        }
        return new Response(json_encode($result));
    }

  public function vaciarTable(){   
        $em = $this->getDoctrine()->getManager();
        $classMetaData = $em->getClassMetadata(TaskJira::Class);
        $connection = $em->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->beginTransaction();
        try {
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $q = $dbPlatform->getTruncateTableSql($classMetaData->getTableName());
            $connection->executeUpdate($q);
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            $connection->commit();
        }
        catch (\Exception $e) {
            $connection->rollback();
        }  

  }
}

