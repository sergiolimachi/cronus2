<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
/**
 * @Route("/homes")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home_principal", methods={"GET"})
     */
    public function index(): Response
    {
        $user = $this->getDoctrine()
        ->getRepository(User::class)->find(1);
        
        return $this->render('grxtemplate/view/layout.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
