<?php
namespace App\Controller;

use App\Entity\Customer;
use App\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
//use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;
/**
 * Customer controller.
 * @Route("/customer")
 */
class CustomerController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/", name="customer")
     * @Template("grxtimetracker/customer/index.html.twig")
     */
    public function index()
    {
        if ($this->security->isGranted('ROLE_SUPER_ADMIN')) {
            return array();
        }else{
            return $this->redirectToRoute('timeentry');
        }
    }

    /**
     * @Route("/list/", name="customer_list" , methods={"GET"})
     * @Template("grxtimetracker/customer/list.html.twig")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository(Customer::class)->findAll();
        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/save", name="customer_save")
     * @Method("POST")
     */
    public function saveAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = null;
        $id = $request->get('id');
        if($id){
            $entity = $em->getRepository(Customer::class)->find($id);
            $entity->setModified(new \DateTime());
        }
        if(!$entity) {
            $entity = new Customer();
            $entity->setCreated(new \DateTime());
            $entity->setModified(new \DateTime());
        }
        $entity->setName($request->get('name'));
        $entity->setEmail($request->get('email'));
        $em->persist($entity);
        $em->flush();

        $projects = $request->get('projects');
        if($projects)
        {
            foreach($projects as $project)
            {
                $entityProject = $em->getRepository(Project::Class)->findOneBy(array('id'=> $project));
                $entityProject->setCustomer($entity);
                $em->persist($entityProject);
                $em->flush();
            }
        }

        return new JsonResponse(array('status'=> true, 'id' => $entity->getId(), 'name' => $entity->getName() ));
    }

    /**
     * Deletes a Customer entity.
     * @param Request $request
     * @param integer $id
     * @Route("/delete/{id}/", name="customer_delete")
     * @Method("GET")
     * @return array Json
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository(Customer::class)->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Customer entity.');
        }
        
        $em->remove($entity);
        $em->flush();
        return new JsonResponse(array('status'=> true));
    }


    /**
     * List all project of user
     * @Route("/list/available/", name="customer_available_list")
     * @Method("GET")
     * @return Object JsonResponse
     */
    public function listAvailableAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository(Customer::class)->getAllCustomerOrderBy();

        if($entities)
        {
            $responseData =  array();
            $cont = 0;
            foreach($entities as $item)
            {
                $responseData[$cont]['id'] = $item->getId();
                $responseData[$cont]['name'] = $item->getName();
                $cont++;
            }
            return new JsonResponse(array('data' => $responseData));
        }else
            return new JsonResponse(array('empty' => true));

    }
    /**
     * List all project of user
     * @Route("/list/with_project/", name="customer_with_project_list")
     * @Method("GET")
     * @return Object JsonResponse
     */
    public function listWithProjectAction()
    {
        $em = $this->getDoctrine()->getManager();
        /* $user = $this->getUser();
        $userId = $user->getId(); */
        $entities = $em->getRepository(Customer::Class)->getAllCustomerOrderBy();

        if($entities)
        {
            $responseData =  array();
            $cont = 0;
            foreach($entities as $item)
            {
                $responseData[$cont]['id'] = $item->getId();
                $responseData[$cont]['name'] = $item->getName();
                $projects = $em->getRepository(Project::Class)->findBy(array('customer' => $item->getId()));
                if($projects)
                {
                    $responseData[$cont]['projects'] = '';
                    $i = 0;
                    $lnt = count($projects);
                    foreach($projects as $project)
                    {
                        $responseData[$cont]['projects'] .= $project->getId().'-';
                        if ($i == $lnt - 1) {
                            $responseData[$cont]['projects'] .= '0';
                        }
                        $i++;
                    }

                }
                $cont++;
            }
            return new JsonResponse(array('data' => $responseData));
        }else
            return new JsonResponse(array('empty' => true));

    }

}
