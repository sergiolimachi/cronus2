<?php
namespace App\Controller;

use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use App\Entity\TimeEntry;
use App\Entity\Customer;
use App\Entity\Project;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Customer controller.
 * @Route("/report")
 */
class ReportController extends AbstractController
{
    private function timeToDecimal($time) {
        $decTime = 0;
        $hms = explode(":", $time);

        if($hms[0] < 1)
        {
            if (preg_match("/^([0-9][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $time, $matches)) {
                $decTime = ($matches[1]*60) + ($matches[2]) + ($matches[3]/60);
            }
            $decTime = gmdate("H.i", ($decTime * 60));

            if($decTime < 1)
                $decTime = $decTime * 1;
        }else{
            $decTime = round($hms[0] + ($hms[1]/60) + ($hms[2]/3600), 2);
        }

        return $decTime;
    }

    private function sumTime($time1, $time2) {
        $times = array($time1, $time2);
        $seconds = 0;
        foreach ($times as $time)
        {
            list($hour,$minute,$second) = explode(':', $time);
            $seconds += $hour*3600;
            $seconds += $minute*60;
            $seconds += $second;
        }
        $hours = (floor($seconds/3600) );
        $seconds -= $hours*3600;
        $minutes  = floor($seconds/60);
        $seconds -= ($minutes*60);

        if($hours <= 9)
            $hours = "0".$hours;

        if($minutes <= 9)
            $minutes = "0".$minutes;

        if($seconds <= 9)
            $seconds = "0".$seconds;

        return "$hours:$minutes:$seconds";
    }

    function createDateRangeArray($strDateFrom,$strDateTo)
    {
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.
        // could test validity of dates here but I'm already doing
        // that in the main script
        $aryRange=array();
        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange,date('Y-m-d',$iDateFrom));
            }
        }
        return $aryRange;
    }

    private function generateData($date, $project, $customer,$type = 1)
    {
        $response = array();
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $date = explode(" ", $date);
        if (sizeof($date) > 1 )//cuando le mandamos fecha-------------------
        {
            $start = date('Y-m-d',strtotime($date[0]));
            $end = date('Y-m-d',strtotime($date[2]));
            $arrayRange = $this->createDateRangeArray($start,$end);

           // dd($arrayRange);//saco 20 hasta 21
            $date1 = explode("-", $date[0]);
            $date2 = explode("-", $date[2]);
            if((count($arrayRange) > 26) and ($date2[1] != $date1[1]) )//-------------------
            {
                $weekMode = true;
                $select = "SELECT WEEK(timeentry.`start`)+1 AS week, CONCAT(WEEK(timeentry.`start`),'/',YEAR(timeentry.`start`)) AS date_time, ";
                $groupBy = "GROUP BY date_time ORDER BY	YEAR (timeentry.`start`) ASC,WEEK (timeentry.`start`) ASC ";
            }else{
                $select = "SELECT DATE_FORMAT(timeentry.`start`,'%Y-%m-%d') as `day`, "
                    ." CONCAT(DAY(timeentry.`start`),'/',MONTH(timeentry.`start`)) AS date_time, ";
                $groupBy = " GROUP BY DAY(timeentry.start)";
            }
        }else{
            $start= trim(date('Y-m-d',strtotime('monday this week')));
            $end = trim(date('Y-m-d',strtotime('sunday this week')));
            $where= " ";
            $arrayRange = $this->createDateRangeArray($start,$end);
            $date1 = explode("-", $start);
            $date2 = explode("-", $end);
            if((count($arrayRange) > 26) and ($date2[1] != $date1[1]) )
            {
                $weekMode = true;
                $select = "SELECT WEEK(timeentry.`start`)+1 AS week, CONCAT(WEEK(timeentry.`start`),'/',YEAR(timeentry.`start`)) AS date_time, ";
                $groupBy = "GROUP BY date_time ORDER BY	YEAR (timeentry.`start`) ASC,WEEK (timeentry.`start`) ASC ";
            }else{
                $select = "SELECT DATE_FORMAT(timeentry.`start`,'%Y-%m-%d') as `day`, "
                    ." CONCAT(DAY(timeentry.`start`),'/',MONTH(timeentry.`start`)) AS date_time, ";
                $groupBy = " GROUP BY DAY(timeentry.start) ";
            }
        }
        $where = " WHERE timeentry.user = ".$user->getId()." AND  timeentry.deletedAt is NULL AND "
            ." timeentry.`start` >= '$start 00:00:00' AND timeentry.`start` <= '$end 23:59:59' ";

        if(isset($project) && $project > 0)
        {
            $where.=" AND timeentry.project = $project ";
            $response['project'] = $project;
        }else
            $response['project'] = 0;

        if(isset($customer) && $customer > 0)
        {
            $response['customer'] = $customer;
            $where.=" AND timeentry.customer = $customer ";
        }else
            $response['customer'] = 0;
            $response['start'] = $start;
            $response['end'] = $end;
            // $select = "SELECT WEEK(timeentry.`start`)+1 AS week, CONCAT(WEEK(timeentry.`start`),'/',YEAR(timeentry.`start`)) AS date_time, ";
            $sql = $select
                ." SEC_TO_TIME( SUM( TIME_TO_SEC(TIMEDIFF( timeentry.stop,timeentry.start)))) AS total_time"
                ." FROM timeentry "
                .$where
                .$groupBy;
                //       $groupBy = "GROUP BY date_time ORDER BY	YEAR (timeentry.`start`) ASC,WEEK (timeentry.`start`) ASC ";
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $entities = $stmt->fetchAll();

        if ($entities) {
            $data = array();
            $dataValid = array();
            $cont = 0;
        //dd($entities);//daran enty     "day" => "2021-11-14"  ///SACA SIEMPRE 5 OBJETOS de 0 a 4
                                //"date_time" => "14/11"
                                //"total_time" => "06:00:12"
            //dd($arrayRange);//array:7 [ 0 => "2021-11-15" 1 => "2021-11-16"
            foreach ($arrayRange as $val) {
                $timestamp = strtotime($val);//convertimoa fecha Fecha pasada '15 May 2015' -->1431640800
                $day = date('D', $timestamp);//"Tue   dia letra

               // $day = $this->get('translator')->trans($day);//-------------------------------------------------------------------------
                foreach ($entities as $entity) { //5 item {day, date_time ,total_time}
                    if (isset($weekMode)) {
                        $dateWeek = new \DateTime($val);
                        $week = $dateWeek->format("W");//num. día de la semana
                        $validWeek = $week.'/'.date('Y',strtotime($val)); //"Tue 16/11"
                        if ($week == $entity['week']) {
                            if(!isset($dataValid[$validWeek]))
                            {
                                $dataValid[$validWeek] = true;
                                $data[$cont]['date_time'] = $entity['date_time'];//"Tue 16/11"
                                $data[$cont]['total_time'] = $this->timeToDecimal($entity['total_time']);
                            }
                        }
                    } else {///columnas grafica

                        //dd($val);//primera fecha 2021-11-11
                       // dd($entity['day']);  //fecha 2021-11-14
                        if ($val == $entity['day']) { 
                            $data[$cont]['date_time'] = $day . ' ' . $entity['date_time'];
                            $data[$cont]['total_time'] = $this->timeToDecimal($entity['total_time']);
                        }
                    }
                    $cont++;
                }
            
                $response['data'] = $data;
            }

            //dd($data); // 78 => array:2 [ "date_time" => "Sun 14/11"  "total_time" => 6.0  ]  85 => array:2 [  "date_time" => "Mon 15/11"  "total_time" => 0.57  ]  92 => array:2 [▼
 
        }

        if(isset($weekMode))
            $response['week_mode'] = true;

        switch($type) {
            case 1: //summary
                $fromList = "SELECT IF(timeentry.project is null, 'No Project', project.name) as projectName, ";
                $sql = $fromList
                    ." SEC_TO_TIME( SUM( TIME_TO_SEC(TIMEDIFF( timeentry.stop,timeentry.start)))) as total_time"
                    ." FROM timeentry LEFT JOIN project ON timeentry.project = project.id"
                    .$where
                    ." GROUP BY projectName ORDER BY projectName ASC";
                break;
            case 2: //detailed
                $fromList = "SELECT timeentry.description, IF(timeentry.project is null, 'No Project', project.name) as projectName, timeentry.start, timeentry.stop,";
                $sql = $fromList
                    ." SEC_TO_TIME( SUM( TIME_TO_SEC(TIMEDIFF( timeentry.stop,timeentry.start)))) as total_time"
                    ." FROM timeentry LEFT JOIN project ON timeentry.project = project.id"
                    .$where
                    ." GROUP BY projectName, timeentry.id ORDER BY projectName ASC, total_time DESC ";
                break;
        }
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $entitiesActivity = $stmt->fetchAll();

        if($entitiesActivity)
        {
            $response['totalTime'] = "00:00:00";
            $data = array();
            $cont = 0;
            foreach($entitiesActivity as $entity)
            {
                if($type == 2)
                {
                    $data[$cont]['description'] = $entity['description'];
                    $data[$cont]['start'] = $entity['start'];
                    $data[$cont]['stop'] = $entity['stop'];
                }
                $data[$cont]['projectName'] = $entity['projectName'];
                $data[$cont]['total_time'] = $entity['total_time'];
                $data[$cont]['number_time'] = $this->timeToDecimal($entity['total_time']);
                $response['totalTime'] = $this->sumTime($response['totalTime'],$entity['total_time']);
                $cont++;
            }
            $response['dataList'] = $data;
        }

        return $response;
    }

    /**
     * @Route("/summary/", name="summary")
     * @Template("grxtimetracker/report/Summary/index.html.twig")
     */
    public function summaryAction(Request $request)
    {
        $date = $request->get('date');
        $project = $request->get('project');
        $customer = $request->get('customer');
        return $this->generateData($date,$project,$customer);
    }

    /**
     * @Route("/detailed/", name="detailed")
     * @Template("grxtimetracker/report/Detailed/index.html.twig")
     */
    public function detailedAction(Request $request)
    {
        $date = $request->get('date');
        $project = $request->get('project');
        $customer = $request->get('customer');
        return $this->generateData($date,$project,$customer,2);
    }
    
    /**
     * @Route("/print/", name="print")
     */
    public function printAction(Pdf $pdf,Request $request)
    {
        $response = array();
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $response['img_bar'] = $request->get('img_bar');
        $response['img_pie'] = $request->get('img_pie');//------------------------------
        $date = $request->get('date');
        $type = $request->get('type');
        $project = $request->get('project');
        $customer = $request->get('customer');
        $date = explode(" ", $date);
        if(sizeof($date) > 1 )
        {
            $start = date('Y-m-d',strtotime($date[0]));
            $end = date('Y-m-d',strtotime($date[2]));
        }else{
            $start= trim(date('Y-m-d',strtotime('monday this week')));
            $end = trim(date('Y-m-d',strtotime('sunday this week')));
        }


        $where = " WHERE timeentry.user = ".$user->getId()." AND  timeentry.deletedAt is NULL AND "
            ." timeentry.`start` >= '$start 00:00:00' AND timeentry.`start` <= '$end 23:59:59' ";

        if(isset($project) && $project > 0)
        {
            $where.=" AND timeentry.project = $project ";
            $response['project'] = $project;
        }else
            $response['project'] = 0;

        if(isset($customer) && $customer > 0)
        {
            $response['customer'] = $customer;
            $where.=" AND timeentry.customer = $customer ";
        }else
            $response['customer'] = 0;

        $response['start'] = $start;
        $response['end'] = $end;
        
        switch($type) {
            case 1: //summary
                $fromList = "SELECT IF(timeentry.project is null, 'No Project', project.name) as projectName, ";
                $sql = $fromList
                    ." SEC_TO_TIME( SUM( TIME_TO_SEC(TIMEDIFF( timeentry.stop,timeentry.start)))) as total_time"
                    ." FROM timeentry LEFT JOIN project ON timeentry.project = project.id"
                    .$where
                    ." GROUP BY projectName ORDER BY projectName ASC";
                
                $nameFile = "report_summary_";
                $printTemplate = 'grxtimetracker/report/Summary/print.html.twig'; 
                break;
            case 2: //detailed
                $fromList = "SELECT timeentry.description, IF(timeentry.project is null, 'No Project', project.name) as projectName, timeentry.start, timeentry.stop,";
                $sql = $fromList
                    ." SEC_TO_TIME( SUM( TIME_TO_SEC(TIMEDIFF( timeentry.stop,timeentry.start)))) as total_time"
                    ." FROM timeentry LEFT JOIN project ON timeentry.project = project.id"
                    .$where
                    ." GROUP BY projectName, timeentry.id ORDER BY projectName ASC, total_time DESC ";
                
                $nameFile = "report_detailed_";
                $printTemplate = 'grxtimetracker/report/Detailed/print.html.twig';
                break;
        }

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $entitiesActivity = $stmt->fetchAll();

        if($entitiesActivity)
        {
            $response['totalTime'] = "00:00:00";
            $data = array();
            $cont = 0;
            foreach($entitiesActivity as $entity)
            {
                if($type == 2)
                {
                    $data[$cont]['description'] = $entity['description'];
                    $data[$cont]['start'] = $entity['start'];
                    $data[$cont]['stop'] = $entity['stop'];
                }
                $data[$cont]['projectName'] = $entity['projectName'];
                $data[$cont]['total_time'] = $entity['total_time'];
                $data[$cont]['number_time'] = $this->timeToDecimal($entity['total_time']);
                $response['totalTime'] = $this->sumTime($response['totalTime'],$entity['total_time']);
                $cont++;
            }
            $response['dataList'] = $data;
        }
        $response['type'] = $type;

        $html = $this->renderView($printTemplate,$response);
        return new PdfResponse($pdf->getOutputFromHtml($html),$nameFile.$start."_".$end.".pdf");
    }

 
    /**
     * @Route("/customer_rep/", name="customer_rep")
     * @Template("grxtimetracker/report/Customer/index.html.twig")
     */
    public function customerAction(Request $request)
    {
        $date = $request->get('date');
        $customer = $request->get('customer');
        return $this->generateData2($date,$customer);
    }

    private function generateData2($date,$customer)
    {
        $response = array();
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $where = " ";
        $whereSub = " WHERE ";
        if(isset($customer) && $customer > 0){
            $response['customer'] = $customer;
            $whereSub=" WHERE t.customer = ".$customer." AND " ;
        }
        $date = explode(" ", $date);
        if (sizeof($date) > 1 ) 
        {
            $start = date('Y-m-d',strtotime($date[0])); 
            $end = date('Y-m-d',strtotime($date[2]));
            $where = $whereSub." t.deletedAt is NULL  AND t.start >='$start 00:00:00'AND t.start <='$end 23:59:59' ";
        }else{
            $start= trim(date('Y-m-d',strtotime('monday this week')));
            $end = trim(date('Y-m-d',strtotime('friday this week')));
            $where = $whereSub." t.deletedAt is NULL  AND t.start >='$start 00:00:00'AND t.start <='$end 23:59:59' ";
        }

//GRAFICA COLUMN 1
        $sql =" SELECT t.start as fecha ,DAYOFWEEK(t.start) as `day`,      
                SUM(CEIL((unix_timestamp(t.stop) - unix_timestamp(t.start))/(60*30))*(60*30)/3600) as `total_time`
                FROM timeentry t  ".$where." 
                GROUP BY DAYOFWEEK(t.start) ORDER BY DAYOFWEEK(t.start) ASC ";
         $stmt = $em->getConnection()->prepare($sql);
         $stmt->execute();
         $entities = $stmt->fetchAll();
         $data = array();
         $cont = 0;
         $totalhoras=0.0;
         $response['start'] = $start; 
         $response['end'] = $end;
         $response['customer'] = $customer;

        foreach ($entities as $entity) {  
            $dias = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
            $day = $dias[date('N', strtotime($entity['fecha']))];
            $data[$cont]['date_time'] = $day;
            $data[$cont]['total_time'] = $entity['total_time'];
            $valor2 = $entity['total_time'];
            $totalhoras = (FLOAT)$totalhoras + (FLOAT)$valor2;
            $cont++;
        }
        $response['totalTime'] = $totalhoras; 
        $response['data'] = $data;
        $from ="CEIL((unix_timestamp(t.stop) - unix_timestamp(t.start))/(60*30))*(60*30)/3600 as `Duration` FROM timeentry t"; 
        //LISTA
        $sql2 ="SELECT t.id,u.id as `userId`,CONCAT(u.firstname,' ',u.lastname) as `userName`,weekofyear(t.start) as `WeekOfYear`,dayname(t.start) as `DayOfWeek`,CONCAT(u.firstname,' ',u.lastname) as `resource`,p.name as `project`,c.name as `customer`,t.description,t.start,t.stop,t.billable, 
                ".$from."
                LEFT JOIN user u ON t.user = u.id
                LEFT JOIN project p on t.project = p.id
                LEFT JOIN customer c on t.customer = c.id  ".$where."  ORDER BY  t.start";
        $stmt = $em->getConnection()->prepare($sql2);
        $stmt->execute();
        $entitiesActivity = $stmt->fetchAll();
        $dataList = array();
        $cont = 0;
        foreach($entitiesActivity as $entity){ 
                 $dataList[$cont]['id'] = $entity['id'];
                 $dataList[$cont]['resource'] = $entity['resource'];
                 $dataList[$cont]['project'] = $entity['project'];
                 $dataList[$cont]['description'] = $entity['description'];
                 $dataList[$cont]['start'] = $entity['start'];
                 $dataList[$cont]['stop'] = $entity['stop'];
                 $dataList[$cont]['Duration'] = $entity['Duration'];
             $cont++;
         }
         $response['dataList'] = $dataList;
//GRAFICA TORTA 1
         $sql3 ="SELECT p.name as `project`,".$from."
                LEFT JOIN project p on t.project = p.id
                LEFT JOIN customer c on t.customer = c.id  ".$where." GROUP BY p.name ";
         $stmt = $em->getConnection()->prepare($sql3);
         $stmt->execute();
         $entitiesActivity2 = $stmt->fetchAll();
         $data = array();
         $cont = 0;
         foreach($entitiesActivity2 as $entity){ 
                  $data[$cont]['projectName'] = $entity['project'];
                  $data[$cont]['total_time'] = $entity['Duration'];
                  $data[$cont]['number_time'] = $entity['Duration']; 
              $cont++;
          }
          $response['dataList2'] = $data;

//GRAFICA TORTA 2
          $sql4 ="SELECT u.id as `userId`,CONCAT(u.firstname,' ',u.lastname) as `userName`,".$from."
                LEFT JOIN user u ON t.user = u.id
                LEFT JOIN customer c on t.customer = c.id  ".$where." GROUP BY u.id ";
          $stmt = $em->getConnection()->prepare($sql4);
          $stmt->execute();
          $entitiesActivity3 = $stmt->fetchAll();
          $data = array();
          $cont = 0;
 
          foreach($entitiesActivity3 as $entity){ 
                   $data[$cont]['userName'] = $entity['userName'];
                   $data[$cont]['total_time'] = $entity['Duration'];
                   $data[$cont]['number_time'] = $entity['Duration']; 
               $cont++;
           }
           $response['dataList3'] = $data;
    return $response;
    }
}
 