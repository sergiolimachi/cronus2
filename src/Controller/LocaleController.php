<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class LocaleController extends AbstractController
{
    /**
     * @Route("/setLocale/{_locale}/", name="locale")
     */
    public function setAction(Request $request)
    {
        return $this->redirect($request->headers->get('referer'), 301);
    }

}
