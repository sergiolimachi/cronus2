<?php

namespace App\Controller;

use App\Entity\TimeEntry;
use App\Entity\Customer;
use App\Entity\Project;
use App\Form\TimeEntryType;
use App\Service\TimeEntryService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * TimeEntry controller.
 *
 * @Route("/timeentry")
 */
class TimeEntryController extends AbstractController
{
    /**
     * Lists all TimeEntry entities.
     * @Route("/", name="timeentry")
     * @Method("GET")
     * @Template("grxtimetracker/timeEntry/index.html.twig")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository(TimeEntry::class)->findAll();


        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new TimeEntry entity.
     * @Route("/", name="timeentry_create")
     * @Method("POST")
     * @Template("grxtimetracker/TimeEntry/new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new TimeEntry();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('timeentry_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    } 

    /**
     * Creates a form to create a TimeEntry entity.
     *
     * @param TimeEntry $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TimeEntry $entity) {
        $form = $this->createForm(new TimeEntryType(), $entity, array(
            'action' => $this->generateUrl('timeentry_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TimeEntry entity.
     * @Route("/new", name="timeentry_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new TimeEntry();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a TimeEntry entity.
     * @Route("/{id}", name="timeentry_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(TimeEntry::Class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TimeEntry entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }
    
    /**
     * @Route("/timer/list", name="timeentry_list")
     * @Method("GET")
     */
    public function listAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $start = date("Y-m-d 00:00:00", strtotime("Monday this week"));
        $finish = date("Y-m-d 23:59:59", strtotime("Sunday this week"));
        $currentWeek = (int)$request->get('week',0);
        if($currentWeek > 0)
        {
            $days = $currentWeek * 7;
            $start = date("Y-m-d 00:00:00", strtotime("-$days days", strtotime($start)));
            $finish = date("Y-m-d 23:59:59", strtotime("-$days days", strtotime($finish)));
        }
        $entities = $em->getRepository(TimeEntry::Class)->getWeekByUser($user->getId(), $start, $finish);
        return $this->render('grxtimetracker/timeEntry/list.html.twig', array(
            'entities' => $entities,
            'week' => $currentWeek,
            'start' => $start,
            'finish' => $finish
        ));
    }


  /**
     * @Route("/timer/start", name="timeentry_start")
     * @Method("POST")
     */ 
    public function startAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $serv = new TimeEntryService($this->getUser(), $em);
        
        $id = $request->get('id');
        if(!empty($id)){
            $entity = $em->getRepository(TimeEntry::Class)->find($id);
        }
        
        $resp = array();
        if(empty($entity)){
            $id = $serv->start($request->get('description'), $request->get('start'), $request->get('tags'), $request->get('billable'),$request->get('project'),$request->get('customer'));
            $resp['id'] = $id;
        }else{
            $id = $serv->start($entity->getDescription(), $request->get('start'), $entity->getTags(), $entity->getBillable(),$entity->getProject(),$entity->getCustomer());
            $resp['id'] = $id;
            $resp['description'] = $entity->getDescription();
        }
        return new JsonResponse($resp);
    }

    /**
     * @Route("/timer/stop", name="timeentry_stop")
     * @Method("POST")
     */
    public function stopAction(Request $request) {
        $serv = new TimeEntryService($this->getUser(), $this->getDoctrine()->getManager());

        $id = $serv->stop($request->get('id'), $request->get('time'),$request->get('description'), $request->get('tags'), $request->get('project'), $request->get('customer'));

        return new JsonResponse(array('id' => $id));
    }

        /**
     * @Route("/timer/save", name="timeentry_save")
     * @Method("POST")
     */
    public function saveAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $timeService = new TimeEntryService($this->getUser(), $em);

        $entity = null;
        $id = $request->get('id');
        if($id){
            $entity = $em->getRepository(TimeEntry::Class)->find($id);
        }
        if(!$entity) {
            $entity = new TimeEntry();
        }
        
        $user = $this->getUser();
        $start = date( "Y-m-d H:i:s", strtotime( $request->get('start')) );
        $stop = date( "Y-m-d H:i:s", strtotime( $request->get('stop')) );

        $entity->setDescription($request->get('description'));
        $entity->setStart(new \DateTime($start));
        $entity->setStop(new \DateTime($stop));
        $entity->setTags($request->get('tags'));
        $billable = $request->get('billable');
        if($billable != null && $billable == 'on'){
            $entity->setBillable(true);
        } else {
            $entity->setBillable(false);
        }
        $entity->setUser($user);
        $entity->setCreated(new \DateTime());
        $entity->setModified(new \DateTime());
        $customer = $request->get('customer');
        if($customer)
        {
            $entityCustomer= $em->getRepository(Customer::Class)->findOneBy(array('id'=> $customer));
            $entity->setCustomer($entityCustomer);
        }

        $project = $request->get('project');
        if($project)
        {
            $entityProject = $em->getRepository(Project::Class)->findOneBy(array('id'=> $project));
            $entity->setProject($entityProject);
        }

        $timeService->save($entity);
        return new JsonResponse(array('id' => $entity->getId()));
    }
   
    /**
     * @Route("/timer/runningentry", name="runningentry")
     * @Method("POST")
     */
    public function runningEntryAction(Request $request) {//  $currentWeek = (int)$request->get('week',0);
        $serv = new TimeEntryService($this->getUser(), $this->getDoctrine()->getManager());
        $entity = $serv->getRunningEntry($request->get('now')); 
        $resp = array();

        if ($entity) {
            $resp = $entity->toArray();
            $fecha = $request->get('now');
            $now = \DateTime::createFromFormat('d/m/Y H:i:s', $fecha);
            $resp['status'] = 'ok';
            $resp['rt'] = $now->getTimestamp() - $entity->getStart()->getTimestamp();
        } else {
            $resp['status'] = 'nook';
        }       
        return new JsonResponse($resp);
    }
    
    /**
     * @Route("/timer/runningentry/update", name="runningentry_update")
     * @Method("POST")
     */
    public function updateRunningEntryAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $serv = new TimeEntryService($this->getUser(), $em);
        
        $entity = null;
        $id = $request->get('id');
        if($id){
            $entity = $em->getRepository(TimeEntry::Class)->find($id);
        }
        if(!$entity) {
            $entity = new TimeEntry();
        }

        $description = $request->get('description');
        if($description)
            $entity->setDescription($description);

        $customer = $request->get('customer');
        if($customer)
        {
            $entityCustomer= $em->getRepository(Customer::Class)->findOneBy(array('id'=> $customer));
            $entity->setCustomer($entityCustomer);
        }

        $project = $request->get('project');
        if($project)
        {
            $entityProject = $em->getRepository(Project::Class)->findOneBy(array('id'=> $project));
            $entity->setProject($entityProject);
        }

        $tags = $request->get('tags');
        if($tags){
            $entity->setTags($request->get('tags'));
        }
        $serv->save($entity);

        return new JsonResponse(array(
            'id' => $entity->getId(),
            'description' => $entity->getDescription()
        ));
    }



    /**
     * Deletes a time entry entity.
     * @param Request $request
     * @param integer $id
     * @Route("/delete/{id}/", name="timeentry_delete")
     * @Method("GET")
     * @return array Json
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository(TimeEntry::Class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Customer entity.');
        }

        $em->remove($entity);
        $em->flush();
        return new JsonResponse(array('status'=> true));
    }

    /**
     * @Route("/{ids}/deletebatch", name="timeentry_delete_batch")
     * @Method("POST")
     */
    public function deleteBatchAction($ids) {
        $serv = new TimeEntryService($this->getUser(), $this->getDoctrine()->getManager());

        $serv->deleteBatch($ids);
        
        return new JsonResponse(array('status' => 'ok'));
    }
}
